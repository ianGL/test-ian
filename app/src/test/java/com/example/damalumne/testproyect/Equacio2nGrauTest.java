package com.example.damalumne.testproyect;

import org.junit.Test;

import static org.junit.Assert.assertEquals;





public class Equacio2nGrauTest {

    @Test(expected=Exception.class)
    public void testEquacio2nGrauExcepcion() throws Exception {
        Equacio2nGrau equacio2 = new Equacio2nGrau(0,3,4);
    }

    @Test
    public void testEquacio2nGrau() throws Exception {
        Equacio2nGrau equacio2 = new Equacio2nGrau(2,3,4);
    }

    @Test
    public void testimprimeixSolucio() throws Exception {
        Equacio2nGrau equacio1 = new Equacio2nGrau(2,3,4);
        Equacio2nGrau equacio2 = new Equacio2nGrau(1,-5,4);
        Equacio2nGrau equacio3 = new Equacio2nGrau(1,2,1);
        assertEquals("No existe solucion en los reales", equacio1.imprimeixSolucio());
        assertEquals("Las dos soluciones son reales: valor1= 1.0 y valor2= 4.0", equacio2.imprimeixSolucio());
        assertEquals("La equacion solo tiene una solucion real: valor1= -1.0", equacio3.imprimeixSolucio());
    }


}
