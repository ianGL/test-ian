package com.example.damalumne.testproyect;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class MyUtilsTest {

    @Test
    public void testEsPrimo() {
        assertFalse(MyUtils.esPrimo(1000));
        assertEquals(true, MyUtils.esPrimo(1));
        assertEquals(true, MyUtils.esPrimo(2));
        assertTrue(MyUtils.esPrimo(-13));
        assertTrue(MyUtils.esPrimo(0));
    }

    @Test
    public void testInvierteCadena() {
        assertEquals("ALOH", MyUtils.invierteCadena("HOLA"));
        assertEquals("aloh", MyUtils.invierteCadena("hola"));
        assertEquals("ALOh", MyUtils.invierteCadena("hOLA"));
        assertEquals("AlOh", MyUtils.invierteCadena("hOlA"));
        assertEquals("", MyUtils.invierteCadena(""));
    }

    @Test
    public void testcalculaEdad() {
        assertEquals(0, MyUtils.calculaEdad(1,1,2019));
        assertEquals(1, MyUtils.calculaEdad(1,5,2018));
        assertEquals(0, MyUtils.calculaEdad(30,1,2018));

    }
}
