package com.example.damalumne.testproyect;

public class Equacio2nGrau {
    double a,b,c;
    double[] solucio;

    public Equacio2nGrau(double a, double b, double c) throws Exception {
        this.a = a;
        this.b = b;
        this.c = c;
        solucio = new double[2];

        if(a == 0) {
            throw new Exception("Los valores no pueden ser igual a 0.");
        }
    }

    public String imprimeixSolucio() {
        String mess;
        double valor1,valor2;
        double formulilla = (b*b)-4 * a * c;

        if(formulilla > 0) {
            valor1 = ((-b) + Math.sqrt(formulilla)) / (2 * a);
            valor2 = ((-b) - Math.sqrt(formulilla)) / (2 * a);
            if(valor1 < valor2)
                mess = "Las dos soluciones son reales: valor1= " + valor1 + " y valor2= " + valor2;
            else
                mess = "Las dos soluciones son reales: valor1= " + valor2 + " y valor2= " + valor1;
        }
        else if(formulilla == 0){
            valor1 = (-b) / (2 * a);
            mess = "La equacion solo tiene una solucion real: valor1= " + valor1;
        }
        else
            mess = "No existe solucion en los reales";

        return mess;
    }
}
