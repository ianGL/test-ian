package com.example.damalumne.testproyect;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class equacionActivity extends AppCompatActivity {

    private EditText numA;
    private EditText numB;
    private EditText numC;
    private TextView solucion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equacion);
    }

    public void pushButton_calcular(View view) throws Exception {
        numA = (EditText) findViewById(R.id.a);
        numB = (EditText) findViewById(R.id.b);
        numC = (EditText) findViewById(R.id.c);
        solucion = (TextView) findViewById(R.id.resultado);
        try {
            Equacio2nGrau eq = new Equacio2nGrau(Double.parseDouble(numA.getText().toString()), Double.parseDouble(numB.getText().toString()), Double.parseDouble(numC.getText().toString()));
            solucion.setText(eq.imprimeixSolucio());
        }catch(Exception ex) {
            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            "No pueden haber valores vacios y A no puede ser 0", Toast.LENGTH_SHORT);

            toast1.show();
        }
    }
}
