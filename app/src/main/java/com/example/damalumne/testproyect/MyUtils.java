package com.example.damalumne.testproyect;

import android.support.v4.app.ServiceCompat;

public class MyUtils {

    public static boolean esPrimo(int number) {
        boolean prime = true;
        for(int i = 2; i < number; i++) {
            if (number % i == 0) {
                prime = false;
                break;
            }
        }
        if (prime)
            return true;
        else
            return false;
    }

    public static String invierteCadena(String cadena) {
        String sCadenaInvertida = "";
        for (int x=cadena.length()-1;x>=0;x--) {
            sCadenaInvertida = sCadenaInvertida + cadena.charAt(x);
        }
        return sCadenaInvertida;
    }

    public static int calculaEdad(int dayBirth, int monthBirth, int yearBirth) {
        return 2019 - yearBirth;
    }
}
