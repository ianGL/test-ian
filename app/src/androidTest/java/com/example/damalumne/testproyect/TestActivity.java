package com.example.damalumne.testproyect;

import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class TestActivity {

    @Rule
    public ActivityTestRule<equacionActivity> activityRule =
            new ActivityTestRule<>(equacionActivity.class, true,     // initialTouchMode
                    true);   // launchActivity

    @Test
    public void testCalcularEcuacion() {
        onView(withId(R.id.a)).perform(typeText("1"));
        onView(withId(R.id.b)).perform(typeText("2"));
        onView(withId(R.id.c)).perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
    }

    @Test
    public void testCalcularEcuacionVacioA() {
        onView(withId(R.id.b)).perform(typeText("2"));
        onView(withId(R.id.c)).perform(typeText("1"),closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
    }

    @Test
    public void testCalcularEcuacionVacioB() {
        onView(withId(R.id.a)).perform(typeText("1"));
        onView(withId(R.id.c)).perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
    }

    @Test
    public void testCalcularEcuacionVacioC() {
        onView(withId(R.id.a)).perform(typeText("1"));
        onView(withId(R.id.b)).perform(typeText("2"), closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
    }

    @Test
    public void testCalcularEcuacionAes0() {
        onView(withId(R.id.a)).perform(typeText("0"));
        onView(withId(R.id.b)).perform(typeText("2"));
        onView(withId(R.id.c)).perform(typeText("1"), closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
    }
}
